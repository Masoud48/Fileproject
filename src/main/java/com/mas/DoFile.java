package com.mas;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class DoFile implements Filetxt {

	private String txt;
	
	File file=new File("test.txt");
	
	public void writeFile()  {
		
		try{
		
		
		FileWriter fwrite=new FileWriter(file);
		fwrite.write(txt);
		fwrite.flush();
		fwrite.close();
		
		}catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	public void readFille() 
	{
		
		try{
			FileReader fread=new FileReader(file);
			StringBuffer stb=new StringBuffer();
			int numCharsRead;
			char[] charArray=new char[1024];
				
			while ((numCharsRead=fread.read(charArray))>0) 
				{
					stb.append(charArray,0,numCharsRead);
				}
			
			fread.close();
			txt=stb.toString();
			
			}catch (IOException e){
			
				e.printStackTrace();
			}
		
		}

	public String getTxt() {
		return txt;
	}

	public void setTxt(String txt) {
		this.txt = txt;
	}
	
	

}
